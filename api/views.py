from django.shortcuts import render
from rest_framework.decorators import api_view
from .models import Student
from .serialzers import StudentSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import authentication_classes, permission_classes


# Create your views here.
@api_view(['GET', 'POST', 'PUT', 'Delete'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def student_api(request, pk=None):
    if request.method == "GET":
        id = pk

        if id is not None:
            stu = Student.objects.get(pk=id)
            serialzer = StudentSerializer(stu)
            return Response(serialzer.data)

        stu = Student.objects.all()
        serializer = StudentSerializer(stu, many=True)
        return Response(serializer.data)

    if request.method == "POST":
        serialzer = StudentSerializer(data=request.data)
        if serialzer.is_valid():
            serialzer.save()
            return Response({'msg': 'Data Creation Successful'}, status=status.HTTP_201_CREATED)
        return Response(serialzer.errors)

    if request.method == "PUT":
        id = pk
        stu = Student.objects.get(pk=id)
        serializer = StudentSerializer(stu, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Data updation successfully'})
        return Response(serializer.errors)

    if request.method == "DELETE":
        id = pk
        stu = Student.objects.get(pk=id)
        stu.delete()
        return Response({"msg": "Deletion successful"})
